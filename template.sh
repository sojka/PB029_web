#!/bin/sh
# Ze vstupního souboru "$1" a šablony "$2" generuj na standardní výstup
# sestavenou stránku.

# Generuj regex pro rozpoznávání HTML komentářů obsahujících právě text "$1".
cmnt() {
  printf '/<!--\s*%s\s*-->/' "$(escape "$1" | sed 's#/#\\/#g')"
}

# Escapuj text "$1" pro zanesení do regexu.
escape() {
  printf '%s' "$1" | sed 's/[&.[\*^$]/\\&/g'
}

# Generuj příkaz sedu, který HTML komentář s textem "$1" nahradí textem "$2".
cmnt_replace_span() {
  printf '%s' "s$(
    cmnt "$1"
  )$(
    escape "$2" | sed -e 's#/#\\/#g'
  )/g"
}

# Generuj příkaz sedu, který HTML komentář s textem "$1" nahradí textem "$2"
# zarovnaným podle indentu komentáře.
cmnt_replace_block() {
  printf '%s' "s$(
    cmnt "$1"
  )$(
    escape "$2" | sed -e 's#/#\\/#g' -e '$!s#$#\\n#' | tr -d '\n'
  )/g"
}

# Zanes mininavigaci do HTML textu "$1".
mininav() {
  MININAVIGACE=\
'<!-- Začátek mininavigace. -->
<h6 class="mininavigace">
    <span class="nezobrazovat_s_CSS">Přejít:</span>
    <a href="#navigation">navigace</a> |
    <a href="#header">na začátek stránky</a> | 
    <a href="#footer">na konec stránky</a>
</h6>
<!-- Konec mininavigace. -->'
  printf '%s' "$TEXT" | sed "
      $(
        cmnt_replace_block 'Návěští sekce.' "$MININAVIGACE

<!-- Návěští sekce. -->"
      )

      $(
        cmnt_replace_block 'Konec textu.' "$MININAVIGACE

<!-- Konec textu. -->"
      )
  "
}

# Generuj navigační menu pro HTML text "$1".
navmenu() {
  local CURR_LEVEL=0
  printf '<!-- Začátek navigačního menu. -->\n'
  (
  # Vytvoř záznam pro PB029.
  printf '1\thttps://sojka.pages.fi.muni.cz/PB029_web/\tPB029\n'
  # Zpracuj kotvičky ze stránky.
  printf '%s' "$1" | sed -n '
    # Ignoruj zakomentované bloky.
    /^\s*<!--\s*$/,/^\s*-->\s*$/ ! p
  ' | sed -n "
    $(cmnt 'Návěští sekce.')"' { # Zpracuj sekce.
      :section-loop # Načti celý nadpis.
      /<\/h[1-6]>/ ! { N ; b section-loop }
      # Přetransformuj nadpis do strojově čitelné podoby.
      s:.*<h\([1-6]\)>[^<]*<span id="\([^"]*\)">\s*\([^\n]*\)\s*</span>.*$:\1\t#\2\t\3:p
    }
  
    /<div class="week"/ { # Zpracuj týdny.
      s:.*<div class="week" id="week\([0-9]*\)".*:3\t#week\1\t\1.\&thinsp;týden:p
    }
  
    /<div class="practice-material"/ { # Zpracuj materiály do cvičení.
      :practice-loop # Načti celý nadpis.
      /<\/h[1-6]>/ ! { N ; b practice-loop }
      # Přetransformuj nadpis do strojově čitelné podoby.
      s:.*<div class="practice-material" id="\([^"]*\)">\s*<h\([1-6]\)>\s*<a href="[^"]*">\s*\([^\n]*\)\s*</a>.*$:\2\t#\1\t\3:p
    }
  '
  # Vytvoř koncový záznam.
  printf '0\t<EOF>\t<EOF>\n'
  ) | while read LEVEL URL TITLE; do
    # Formátuj předzpracovaný vstup do HTML.
    while [ $LEVEL -gt $CURR_LEVEL ]; do
      printf '<ul>\n'
      CURR_LEVEL=$(($CURR_LEVEL + 1))
    done
    while [ $LEVEL -lt $CURR_LEVEL ]; do
      printf '</li></ul>\n'
      eval FIRST_$CURR_LEVEL=''
      CURR_LEVEL=$(($CURR_LEVEL - 1))
    done
    if [ $URL != '<EOF>' ]; then
      [ -z "$(eval echo \$FIRST_$CURR_LEVEL)" ] || printf '</li>'
      printf '<li><a href="%s">%s</a>\n' "$URL" "$TITLE"
      eval FIRST_$CURR_LEVEL=false
    fi
  done
  printf '<!-- Konec navigačního menu. -->\n'
}

# Extrahuj informace ze vstupních souborů.
SABLONA="$(cat "$2")"
ZAHLAVI="$(sed -n "$(cmnt 'Začátek záhlaví.'),$(cmnt 'Konec záhlaví.')p" <"$1")"
TEXT="$(sed -n "$(cmnt 'Začátek textu.')"',$p' <"$1")"
TITULEK="$(
  printf '%s' "$ZAHLAVI" |
  sed 's/<[^>]*>//g' | tr '\n' ' ' | sed 's/^\s*//;s/\s*$//')"

# Generuj výstup.
printf '%s' "$SABLONA" | sed "
  $(
    cmnt_replace_span 'Na toto místo vlož titulek.' "$TITULEK"
  )

  $(
    cmnt_replace_block 'Na toto místo vlož záhlaví.' "$ZAHLAVI"
  )

  $(
    cmnt_replace_block 'Na toto místo vlož text.' "$(
      mininav "$TEXT"
    )"
  )

  $(
    cmnt_replace_block 'Na toto místo generuj navigační menu.' "$(
      navmenu "$TEXT"
    )"
  )
" | vlna -x 266E6273703B -v KkSsVvZzOoUuAaIi -m -n -f -s
