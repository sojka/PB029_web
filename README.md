# PB029 Web

This repository contains the source code of [the web site][2] of [the PB029 course][1] taught at the Faculty of Informatics, Masaryk University. See [a separate article][3] (in Czech) that describes how you can contribute.

 [1]: https://is.muni.cz/auth/predmet/fi/PB029
 [2]: https://sojka.pages.fi.muni.cz/PB029_web/
 [3]: https://sojka.pages.fi.muni.cz/PB029_web/practices/sprava-verzi/#nahlasovani-chyb-na-webu
