# LaBAK moduly nepodporuje.
if [ `hostname` != 'labak.fi.muni.cz' ]; then
    # Moduly pripojovane vsude.
    module add texlive jpeg2ps sp xml-catalogs
    # Moduly pripojovane na Anxuru a Aise.
    if [ `hostname` = 'anxur.fi.muni.cz' -o `hostname` = 'aisa.fi.muni.cz' ]; then
        module add xpdf acroread subversion
    fi
    # Moduly pripojovane na Ubuntu.
    if grep -i ubuntu /etc/lsb-release &>/dev/null; then
        module add xpdf acroread
    fi
fi
